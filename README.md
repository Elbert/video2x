## Introduction

Video2X is a video/GIF/image upscaling and frame interpolation software written in Python. It can use these following state-of-the-art algorithms to increase the resolution and frame rate of your video/GIF/image. More information about the algorithms that it supports can be found in [the documentations](https://github.com/k4yt3x/video2x/wiki/Algorithms).

## This fork

This is a personal fork which just changes the video and audio codec used as I need AV1 und Opus. See the [original repository on GitHub](https://github.com/k4yt3x/video2x).

## License

This project is licensed under the [GNU Affero General Public License Version 3 (GNU AGPL v3)](https://www.gnu.org/licenses/agpl-3.0.txt)
Copyright (c) 2018-2023 K4YT3X and contributors.

More licensing information can be found in the [NOTICE](licenses/NOTICE) file.
